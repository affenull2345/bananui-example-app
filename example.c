/* Example app for bananui - license: CC0 */
#include <linux/input-event-codes.h>
#include <bananui/bananui.h>
#include <stdio.h>

enum view_state { VIEW_GREETING, VIEW_BUTTON };

struct app_state {
	enum view_state vs;
	int clicked;
};

void showButton(bWindow *wnd)
{
	struct app_state *state;
	state = bGetWindowData(wnd);
	state->vs = VIEW_BUTTON;
	bClearWnd(wnd);
	bCreateButton(wnd, "@call-start@Phone Button", NO_WIDGET);
	bSetSoftkeys(wnd, "", "CLICK", "");
	bRefreshWnd(wnd, 0);
}

void showGreeting(bWindow *wnd)
{
	struct app_state *state;
	state = bGetWindowData(wnd);
	state->vs = VIEW_GREETING;
	bClearWnd(wnd);
	bCreateLabel(wnd, "Hello World!", TA_CENTER, NO_WIDGET);
	if(state->clicked){
		state->clicked = 0;
		bCreateLabel(wnd, "Phone button clicked", TA_CENTER, NO_WIDGET);
	}
	bSetSoftkeys(wnd, "", "SHOW BUTTON", "");
	bRefreshWnd(wnd, 0);
}

int eventCallback(bEventType type, bEventData *data)
{
	struct app_state *state;
	state = bGetWindowData(data->window);
	if(type == BEV_CLICK && state->vs == VIEW_BUTTON){
		state->clicked = !state->clicked;
	}
	else if(type == BEV_KEYDOWN && data->val == KEY_ENTER &&
		state->vs == VIEW_GREETING)
	{
		showButton(data->window);
	}
	else if(type == BEV_EXIT && state->vs == VIEW_GREETING){
		return 0;
	}
	else if(type == BEV_EXIT && state->vs == VIEW_BUTTON){
		showGreeting(data->window);
	}
	return -1;
}

int main()
{
	struct app_state state;
	state.clicked = 0;
	bWindow *wnd;
	wnd = bCreateWnd();
	if(!wnd){
		fprintf(stderr, "Failed to create window.\n");
		return 1;
	}
	bSetWindowData(wnd, &state);
	showGreeting(wnd);
	bSetEventCallback(wnd, eventCallback);
	return bEventLoop(wnd);
}
