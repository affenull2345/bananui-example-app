OBJECTS = example.o

bananui-example-app: $(OBJECTS)
	$(CC) -o $@ $(OBJECTS) -lbananui

%.o: %.c
	$(CC) -o $@ -c $*.c $(CFLAGS)

clean:
	rm -f $(OBJECTS) bananui-example-app

install:
	install bananui-example-app $(DESTDIR)/usr/bin
	install bananui-example-app.desktop $(DESTDIR)/usr/share/applications
