# Bananui Example App
This is an example app for the [Bananui](https://gitlab.com/affenull2345/bananui-base) user interface, packaged for [Bananian](https://gitlab.com/affenull2345/bananian).

## Building
Requires the Bananian build system to be set up locally.
```
cd [...]/bananian
sudo make package PACKAGE_PATH=[...]/bananui-example-app
scp bananui-example-app*.deb user@[X.X.X.X]: # X.X.X.X is the IP address

ssh user@[X.X.X.X]
# Enter password
sudo apt install ./bananui-example-app*.deb
# Enter password
exit
```
